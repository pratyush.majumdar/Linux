#!/bin/sh
IPTABLES="/sbin/iptables"

# 1. Flush old rules, old custom tables
$IPTABLES --flush
$IPTABLES --delete-chain

# 2. Enable free use of loopback interfaces
$IPTABLES -A INPUT -i lo -j ACCEPT
$IPTABLES -A OUTPUT -o lo -j ACCEPT


# 3. Accept inbound TCP packets
$IPTABLES -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# 4. Allow Local IPs for ssh
$IPTABLES -A INPUT -p tcp --dport 22 -m state --state NEW -s 192.168.0.0/16 -j ACCEPT

# 5. Allow Local IPs for MySQL
$IPTABLES -A INPUT -p tcp --dport 3306 -m state --state NEW -s 192.168.0.0/16 -j ACCEPT

# 6. Allow Skilrock VPN for ssh
$IPTABLES -A INPUT -p tcp --dport 22 -m state --state NEW -s 10.101.101.0/24 -j ACCEPT

# 7. Allow http and https for all
$IPTABLES -A INPUT -p tcp --dport 80 -m state --state NEW -s 0.0.0.0/0 -j ACCEPT
$IPTABLES -A INPUT -p tcp --dport 8080 -m state --state NEW -s 0.0.0.0/0 -j ACCEPT
$IPTABLES -A INPUT -p tcp --dport 443 -m state --state NEW -s 0.0.0.0/0 -j ACCEPT

# 8. Allow ftp for all
$IPTABLES -A INPUT -p tcp --dport 21 -m state --state NEW -s 0.0.0.0/0 -j ACCEPT

# 9. Accept inbound ICMP messages
$IPTABLES -A INPUT -p ICMP --icmp-type 8 -s 0.0.0.0/0 -j ACCEPT
$IPTABLES -A INPUT -p ICMP --icmp-type 11 -s 0.0.0.0/0 -j ACCEPT

# 10. Accept outbound packets
$IPTABLES -I OUTPUT 1 -m state --state RELATED,ESTABLISHED -j ACCEPT

# 11. Logging to /var/log/messages
$IPTABLES -A INPUT -j LOG --log-prefix "INPUT FAIL !!!"
#$IPTABLES -A OUTPUT -j LOG --log-prefix "OUTPUT FAIL !!!"

# 12. Setting OUTOUT chain
$IPTABLES -A OUTPUT -p tcp --dport 21 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 22 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 25 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 80 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 443 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p icmp --icmp-type echo-request -j ACCEPT

# 13. Set default policies for packets not matching any rule
$IPTABLES -P INPUT DROP
$IPTABLES -P FORWARD DROP
$IPTABLES -P OUTPUT DROP


### Install Java, create Symlink(Important) ###

- Download and Extract Java
- sudo ln -s /opt/jdk1.8.0_171/bin/java /usr/bin/java




### Generate SSL Certificates ###

sudo vi /etc/pki/tls/openssl.cnf
Find the [ v3_ca ] section in the file, and add this line under it (substituting 
in the ELK Server's private IP address):
subjectAltName = IP: 192.168.xxx.xxx

Now generate the SSL certificate and private key in the appropriate locations 
(/etc/pki/tls/), with the following commands:  
cd /etc/pki/tls  
sudo openssl req -config /etc/pki/tls/openssl.cnf -x509 -days 3650 -batch -nodes -newkey rsa:2048 -keyout private/logstash-forwarder.key -out certs/logstash-forwarder.crt



### Install Elasticsearch ###

- Download Elasticsearch from https://www.elastic.co/downloads/elasticsearch
- sudo rpm -iVh elasticsearch-6.4.0.rpm
- Configure Elasticsearch ![Coming Soon](https://img.shields.io/badge/Status-Coming%20Soon-red.svg)
- sudo systemctl start elasticsearch
- sudo systemctl enable elasticsearch



### Install Kibana ###

- Download Kibana from https://www.elastic.co/downloads/kibana
- sudo rpm -iVh kibana-6.4.0-x86_64.rpm
- Configure Kibana ![Coming Soon](https://img.shields.io/badge/Status-Coming%20Soon-red.svg)
- sudo systemctl start kibana
- sudo systemctl enable kibana



### Install Logstash ###

- Download Kibana from https://www.elastic.co/downloads/logstash
- sudo rpm -iVh logstash-6.4.0.rpm
- Configure Logstash ![Coming Soon](https://img.shields.io/badge/Status-Coming%20Soon-red.svg)
- sudo systemctl start logstash
- sudo systemctl enable logstash



### Setup Filebeat (For Clients) ###

- Copy logstash-forwarder.crt to /etc/pki/tls/certs/ directory
- Download Filebeat from https://artifacts.elastic.co/downloads/beats
- sudo rpm -iVh filebeat-6.4.0-x86_64.rpm
- Configure Filebeat ![Coming Soon](https://img.shields.io/badge/Status-Coming%20Soon-red.svg)
- sudo systemctl start filebeat
- sudo systemctl enable filebeat



### Sample Kibana Graph(Apache Server) ###
Tutorial on Visualizing graphs is not a part of this installation document

![Screenshot](https://gitlab.com/pratyush.majumdar/Linux/raw/master/elk-installation/APACHE.png)
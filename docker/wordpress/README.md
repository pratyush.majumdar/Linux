# Docker Compose Wordpress Setup

## Prerequisites
1. A server running Ubuntu 18.04, along with a non-root user with sudo privileges and an active firewall.
2. Docker and Docker Compose installed on your server.
3. A registered domain name. This tutorial will use example.com throughout. You can get one for free at Freenom.
4. Both of the following DNS records set up for your server. 
    1. An A record with example.com pointing to your server’s public IP address.
    2. An A record with www.example.com pointing to your server’s public IP address.

```docker
$ docker-compose up -d
```

```docker
Creating db ... done
Creating wordpress ... done
Creating webserver ... done
Creating certbot   ... done
```

```docker
$ docker-compose ps
```

```docker
Name                 Command               State           Ports       
-------------------------------------------------------------------------
certbot     certbot certonly --webroot ...   Exit 0                      
db          docker-entrypoint.sh --def ...   Up       3306/tcp, 33060/tcp
webserver   nginx -g daemon off;             Up       0.0.0.0:80->80/tcp
wordpress   docker-entrypoint.sh php-fpm     Up       9000/tcp
```

Reference: https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-docker-compose